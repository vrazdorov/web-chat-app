export default [
    {
        name: 'register',
        middleware: 'guest',
        url: '/register',
        meta: {},
        controller: './app/contollers/register.ts'
    }, {
        name: 'login',
        middleware: 'guest',
        url: '/login',
        meta: {},
        controller: './app/contollers/login.ts'
    }, {
        name: 'chat',
        middleware: 'auth',
        url: '/chat',
        meta: {},
        controller: './app/contollers/chat.ts'
    }
]