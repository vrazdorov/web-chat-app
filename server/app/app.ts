import Kernel from '../kernel/kernel';
import Router from '../router/route';
import Middlewate from '../middleware/middleware';
import DateBase from '../database/database';

import { IEnv, IDataBase } from '../services/interfaces';

class App {

    private _env: IEnv = <IEnv>{};
    private _confDb: IDataBase = <IDataBase>{};

    private _router: Router;
    private _middleware: Middlewate;
    private _db: DateBase;
    
    constructor(env: IEnv, configDb: IDataBase) {
        this._env = env;
        this._confDb = configDb;
        this._router = new Kernel.Router;
        this._middleware = new Kernel.Middlewate;
        this._db = new Kernel.DateBase;
    }

    public declaration()  { 
        this._router.setMiddleware(this._middleware.init());
        this._router.init(this._env);
        this._db.setConfig(this._confDb);
        this._db.init();
    }

}

export default App;