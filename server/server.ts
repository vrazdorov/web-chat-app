import App from './app/app';
import { ENV, CONFIG } from './env';

new App(ENV, CONFIG).declaration();
