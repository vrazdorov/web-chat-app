export interface IDefaultRouter {
    url: string;
    middleware?: string;
}