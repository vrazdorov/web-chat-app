export interface IEnv {
    PORT: number,
    PREFIX_API: string
}