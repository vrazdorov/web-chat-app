import { IDataBase } from '../services/interfaces';

class DateBase {

    private conf: IDataBase = <IDataBase>{};

    public init(): void {

    }

    public setConfig(conf: IDataBase): void {
        this.conf = conf;
    }

}

export default DateBase;
