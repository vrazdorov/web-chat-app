import Router from '../router/route';
import Middlewate from '../middleware/middleware';
import DateBase from '../database/database';

export default {
    Router,
    Middlewate,
    DateBase
}